var elevationLoader = (function() {

  function buildUrl(latlng1, latlng2, options) {
    if (!options.source) {
      options.source = "cdem";
    } else {
      options.source = options.source.toLowerCase();
    }
    let query = "";
    let type = "altitude";
    if (options.profile) {
      type = "profile";
      let lineString = `LINESTRING(${latlng1.lng} ${latlng1.lat}, ${latlng2.lng} ${latlng2.lat})`;
      /*
      const dist = latlng1.distanceTo(latlng2);
      if (dist > 20) {
        const steps = Math.floor(dist / 20);
        lineString += "&steps=" + steps;
      }
      */
      query = `path=${lineString}`;
    } else {
      query = `lat=${latlng1.lat}&lon=${latlng1.lng}`;
    }
    let url = `http://geogratis.gc.ca/services/elevation/${options.source}/${type}?${query}`;
    return url;
  }

  function getElevation(latlng, options) {
    options = options || {};
    let url = buildUrl(latlng, null, options);
    return new Promise(function(resolve, reject) {
      $.getJSON(url, (json) => {
        //console.log("Elevation json", json);
        resolve(json);
      }).fail(function (jqXHR, textStatus, errorThrown) {
        reject(errorThrown);
      });
    });
  }

  function getElevations(latlngs, options) {
    options = options || {};
    const queue = latlngs.slice(0);
    const maxRequests = Math.min(50, queue.length);
    options.finished = false;
    options.count = 0;
    options.total = latlngs.length;
    for (let i = 0; i < maxRequests; i++) {
      processQueue(queue, options, true);
    }
  }

  function processQueue(queue, options, doit = false) {
    const latlng = queue.shift();
    if (latlng) {
      const handler = function(json) {
        options.count++;
        console.log("Elevation", json.altitude, `${options.count}/${options.total}`);
        try {
          latlng.alt = parseFloat(json.altitude);
        } catch (ex) {}
        options.progressCallback(options.count, options.total);
        if (options.count >= options.total) {
          options.finished = true;
          console.log("** Finished!");
          options.finishedCallback();
        }
        processQueue(queue, options, doit);
      };
      if (doit) {
        getElevation(latlng, options).then(handler);
      } else {
        setTimeout(() => {
          handler({altitude: 0});
        }, Math.round(Math.random() * 500) + 100);
      }
    }
  }

  return {
    loadElevation(latlng, options) {
      return getElevation(latlng, options);
    },
    loadElevations(latlngs, options) {
      return getElevations(latlngs, options);
    }
  }
})();

/*
function loadNextElevation() {
  const max = elevationPoints.length;
  if (elevationIndex < max) {
    const p1 = elevationPoints[elevationIndex - 1];
    const p2 = elevationPoints[elevationIndex];
    if (p1.dist === undefined) {
      p1.dist = 0;
    }
    p2.dist = p1.dist + p2.distanceTo(p1);
    console.log("Loading elevation " + elevationIndex + "/" + max);
    elevationIndex++;
    setProgress(Math.round(100 * elevationIndex / max));

    loadElevation(p1, p2).then((elevations) => {
      console.log("Resolved", elevations);
      if (elevations.length) {
        let latlngs = [];
        for (let i = 0; i < elevations.length; i++) {
          const item = elevations[i];
          const coords = item.geometry.coordinates;
          const latlng = L.latLng(coords[1], coords[0], item.altitude);
          const dist = p1.dist + latlng.distanceTo(p1);
          lastLatLng = latlng;

          latlngs.push({
            latlng,
            dist,
            y: item.altitude,
            x: dist / 1000.0
          });
        }
        latlngs.sort(function(ll1, ll2) {
          if (ll1.dist < ll2.dist) {
            return -1;
          } else if (ll2.dist < ll1.dist) {
            return 1;
          }
          return 0;
        });
        for (let i = 0; i < latlngs.length; i++) {
          latlngs[i].idx = elevationData.length;
          elevationData.push(latlngs[i]);
        }
      } else {
        console.log("Not array!", elevations);
      }

      loadNextElevation();
    });
  } else {
    console.log("Done loading " + max + " elevations, added " + elevationData.length + " data points");
    console.log(JSON.stringify(elevationData));
    setProgress(100);
    setTimeout(() => {
      showProgressBar(false);
      showElevationChart();
    }, 500);
  }
}
 */