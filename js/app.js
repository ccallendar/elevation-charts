
const mapProps = {};
const CDEM_URL = "http://geogratis.gc.ca/services/elevation/cdem/profile?path=";
const CDSM_URL = "http://geogratis.gc.ca/services/elevation/cdsm/profile?path=";

let elevationPoints = [];
let elevationData = [];
let raceStatsData = [];
let elevationIndex = 0;
let progressBarContainer = null;
let progressBar = null;

function ready() {
  progressBarContainer = document.querySelector("#elevationProgressContainer");
  progressBar = document.querySelector("#elevationProgress");
  console.log("Creating map...");
  const map = L.map("map").setView([48.418, -123.364], 15);
  mapProps.map = map;
  //L.tileLayer.provider("Esri.WorldImagery").addTo(map);
  mapProps.tileLayer = L.tileLayer.provider("OpenStreetMap.Mapnik").addTo(map);

  $.getJSON("/data/tc10k-2019-interpolated.json", (data) => {
    const latlngs = data.map((item) => L.latLng(item.lat, item.lng, item.alt));
    console.log("Adding polyline", latlngs.length);
    mapProps.line = L.polyline(latlngs).addTo(map);

    //mapProps.start = L.marker(latlngs[0]).addTo(map);
    //mapProps.end = L.marker(latlngs[latlngs.length - 1]).addTo(map);
  });
}

function loadElevationHandler() {
  elevationPoints = mapProps.line.getLatLngs(); // .slice(0, 50);
  console.log("Loading elevations", elevationPoints.length);
  elevationIndex = 1;
  showProgressBar(true);
  let cdemElevations = [];
  let cdsmElevations = [];
  console.time("cdemElevation");
  loadElevations(elevationPoints, "cdem").then(function(elevations) {
    cdemElevations = elevations;
    window.cdemElevations = elevations;
    console.timeEnd("cdemElevation");
    console.time("cdsmElevation");
    return loadElevations(elevationPoints, "cdsm")
  }).then(function(elevations) {
    cdsmElevations = elevations;
    window.cdsmElevations = elevations;
    elevationData = cdemElevations;
    raceStatsData = cdsmElevations;
    console.timeEnd("cdsmElevation");
    showElevationChart();
  });
}

function loadElevations(latlngs, source) {
  return new Promise(function(resolve, reject) {
    console.log("Loading elevations of type", source, latlngs.length);
    elevationLoader.loadElevations(latlngs, {
      source,
      progressCallback: function(count, total) {
        const percent = Math.min(100, Math.round(count * 100 / total));
        // console.log(`${percent}%`);
        setProgress(percent);
      },
      finishedCallback: function() {
        console.log("Finished!", source);
        setProgress(100);
        setTimeout(() => {
          showProgressBar(false);
        }, 500);
        resolve(latlngsToElevationData(latlngs));
      },
    });
  });
}

function showProgressBar(shown) {
  progressBarContainer.style.display = (shown ? "block" : "none");
  progressBar.classList.remove("finished");
  setProgress(0);
}

function setProgress(percent) {
  progressBar.style.width = percent + "%";
  if (percent >= 100) {
    progressBar.classList.add("finished");
  }
  progressBarContainer.querySelectorAll(".elevation-progress-label").forEach((lbl) => {
    lbl.innerHTML = "Loading " + percent + "%";
  });
}

function showElevationChart() {
  raceMapsElevationChart.createElevationChart("elevationChart", elevationData, raceStatsData,
    function(x, y, idx) {
    if (isNaN(idx)) {
      return;
    }
    const latlng = elevationData[idx].latlng;
    if (mapProps.hoverMarker) {
      mapProps.hoverMarker.setLatLng(latlng)
    } else {
      mapProps.hoverMarker = L.marker(latlng).addTo(mapProps.map);
    }
  }, function() {
    if (mapProps.hoverMarker) {
      mapProps.map.removeLayer(mapProps.hoverMarker);
      delete mapProps.hoverMarker;
    }
  });
}

function latlngsToElevationData(latlngs) {
  const data = [];
  let prev = null;
  for (let i = 0; i < latlngs.length; i++) {
    const latlng = latlngs[i];
    latlng.dist = 0;
    if (prev) {
      latlng.dist = prev.dist + latlng.distanceTo(prev) / 1000.0;
    }
    data.push({
      x: latlng.dist,
      y: latlng.alt,
      idx: i,
      latlng,
    });
    prev = latlng;
  }
  smooth(data, 4);
  return data;
}

function showElevationHandler() {
  $.getJSON("/data/elevation-data.json", (json) => {
    for (let i = 0; i < json.length; i++) {
      json[i].idx = i;
    }
    //const clone = JSON.parse(JSON.stringify(json));
    smooth(json, 4);
    elevationData = json;
    //raceStatsData = clone;
/*
    $.getJSON("/data/elevation-data-racestats2.json", (rsData) => {
      raceStatsData = [];
      let prev = null;
      let dist = 0;
      rsData.forEach((pt) => {
        const ll = L.latLng(pt.lat, pt.lng, pt.alt);
        if (prev) {
          dist += ll.distanceTo(prev);
        }
        prev = ll;
        raceStatsData.push([dist/1000.0, pt.alt]);
      });

      showElevationChart();
    });
 */
    showElevationChart();
  });
}

// From https://github.com/ndelvalle/array-smooth
function smooth(arr, windowSize = 2) {
  for (let i = 0; i < arr.length; i += 1) {
    const leftOffset = i - windowSize;
    const from = leftOffset >= 0 ? leftOffset : 0;
    const to = i + windowSize + 1;
    let count = 0;
    let sum = 0;
    for (let j = from; j < to && j < arr.length; j += 1) {
      sum += arr[j].y;
      count += 1;
    }
    let avg = sum / count;
    arr[i].y2 = arr[i].y;
    arr[i].y = avg;
  }
}