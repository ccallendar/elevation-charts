var raceMapsElevationChart = (function(Highcharts) {

  var charts = {};

  var colors = [ "#BD0026", "#F03B20", "#FD8D3C", "#FEB24C", "#FED976", "#FFFF60", "#D9F0A3", "#ADDD8E", "#78C679", "#31A354", "#006837", "#0000DD" ].reverse();
  var baseZones = [];
  var len = colors.length;
  for (var i = 0; i < colors.length; i++) {
    baseZones.push({ value: (i/len), color: colors[i] });
  }

  function createZones(data) {
    var zones = baseZones.concat();
    var minY = Math.max(0, data.minY);
    var maxY = data.maxY;
    var values = [];
    if (!isNaN(maxY)) {
      // Change color gradient at least every 5m
      var deltaY = Math.max(5, (maxY - minY) / len);
      maxY = Math.max(maxY, minY + (len * deltaY));
      for (i = 0; i < len; i++) {
        var zone = zones[i];
        // Keep blue always at sea level?
        if (i == 0) {
          zone.value = 0;
        } else if (i == (len - 1)) {
          zone.value = Math.ceil(maxY);
        } else {
          zone.value = Math.floor(minY + (i * deltaY));
        }
        values.push(zone.value);
      }
    }
    console.log(data);
    console.log("Zones: ", values);

    return zones;
  }

  function getMinMax(data) {
    var maxX = NaN, minX = NaN, maxY = NaN, minY = NaN;
    for (var i = 0; i < data.length; i++) {
      var pt = data[i];
      var x = pt.x;
      var y = pt.y;
      minX = min(x, minX);
      maxX = max(x, maxX);
      minY = min(y, minY);
      maxY = max(y, maxY);
    }
    return { minX: minX, maxX: maxX, minY: minY, maxY: maxY };
  }

  function setMinMax(data) {
    var mm = getMinMax(data);
    data.minX = mm.minX;
    data.maxX = mm.maxX;
    data.minY = mm.minY;
    data.maxY = mm.maxY;
  }

  function min(value, minValue) {
    return (isNaN(minValue) ? value : Math.min(value, minValue));
  }

  function max(value, maxValue) {
    return (isNaN(maxValue) ? value : Math.max(value, maxValue));
  }

  function reduceDigits(num, digits) {
    return parseFloat(num.toFixed(digits));
  }


  return {

    /**
     * Creates an elevation chart in the given html element.
     * @param {string} elementId the html element id
     * @param {Array} elevationData the data, expected to contain x and y numeric values
     * @param {function} mouseOverPointCallback the function to call when the mouse hovers over a point in the chart
     * @param {function} mouseOutChartCallback the function to call when the mouse leaves the chart
     */
    createElevationChart: function(elementId, elevationData, raceStatsData, mouseOverPointCallback, mouseOutChartCallback) {
      // If the data was converted using the convertData - it will already have the min/max values set
      if (!elevationData.hasOwnProperty("minY")) {
        setMinMax(elevationData);
      }
      let range = elevationData.maxY - elevationData.minY;
      let offset = Math.max(10, (0.2 * range));
      let minY = Math.floor((elevationData.minY - offset) / 10) * 10;
      let maxY = Math.ceil((elevationData.maxY + offset) / 10) * 10;
      console.log("MinY", minY, "MaxY", maxY, "range", range, "Offset", offset);

      if (charts[elementId] != null) {
        // Update the data instead
        console.log("Updating elevation data");
        var chart = charts[elementId];
        chart.series[0].setData(elevationData);

      } else {
        console.log("Creating new Highchart");
        charts[elementId] = Highcharts.chart(elementId, {
          chart: {
            type: 'areaspline'
          },
          title: {
            text: ''
          },
          xAxis: {
            type: 'linear',
            min: 0,
            //max: Math.ceil(elevationData.maxX),
            title: {
              text: 'Distance (km)',
              style: {
                fontWeight: 'bold'
              }
            }
          },
          yAxis: {
            type: 'linear',
            min: Math.max(0, minY),
            max: Math.max(40, maxY),
            title: {
              text: 'Elevation (m)',
              style: {
                fontWeight: 'bold'
              }
            }
          },
          plotOptions: {
            areaspline: {
              fillOpacity: 0.5,
              marker: {
                enabled: false
              }
            },
            series: {
              point: {
                events: {
                  mouseOver: function() {
                    if (mouseOverPointCallback) {
                      mouseOverPointCallback(this.x, this.y, this.idx);
                    }
                  }
                }
              },
              events: {
                mouseOut: function() {
                  if (mouseOutChartCallback) {
                    mouseOutChartCallback();
                  }
                }
              }
            }
          },
          tooltip: {
            headerFormat: '',
            pointFormat: 'Elevation at {point.x:.2f} km: <b>{point.y:.1f} m</b>'
          },
          legend: {
            enabled: false
          },
          credits: {
            enabled: false
          },
          series: [{
            name: 'Elevation Series',
            data: elevationData,
            color: '#B93'
            //zones: createZones(elevationData)
          }, {
            name: 'RaceStats Data',
            data: raceStatsData,
            color: '#39B',
          }]
        });
      }
    },

    clearChart: function(elementId) {
      delete charts[elementId];
    },

    convertData: function(rawData, xField, yField, xScale, yScale) {
      if (xScale == null) {
        xScale = 1;
      }
      if (yScale == null) {
        yScale = 1;
      }
      var data = [];
      // keep track of the min/max values too
      data.minX = NaN;
      data.maxX = NaN;
      data.minY = NaN;
      data.maxY = NaN;
      for (var i = 0; i < rawData.length; i++) {
        var point = rawData[i];
        if ((point.hasOwnProperty(xField) && point.hasOwnProperty(yField))) {
          var x = parseFloat(point[xField]) * xScale;
          var y = parseFloat(point[yField]) * yScale;
          if (!isNaN(x) && !isNaN(y)) {
            // Reduce the number of decimal places
            x = reduceDigits(x, 4);
            y = reduceDigits(y, 1);

            data.push({x: x, y: y, idx: i});
            data.minX = min(x, data.minX);
            data.maxX = max(x, data.maxX);
            data.minY = min(y, data.minY);
            data.maxY = max(y, data.maxY);
          }
        }
      }

      var maxLength = 1000;   // turbo threshold
      if (data.length > maxLength) {
        var step = Math.ceil(data.length / maxLength);
        var subsampled = [];
        for (i = 0; i < data.length; i += step) {
          subsampled.push(data[i]);
        }
        console.log("Reduced elevation data from " + data.length + " down to " + subsampled.length);
        data = subsampled;
      }

      return data;
    }

  };

})(Highcharts);
